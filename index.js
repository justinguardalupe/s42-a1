// Using DOM
// Retrieve an element from webpage
const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');


// Event listener - an interaction between the user and the web page.
/*txtFirstName.addEventListener('keyup',(event) => {
	spanFullName.innerHTML = txtFirstName.value;
})*/

// Assign same event to multiple listeners

/*txtFirstName.addEventListener('keyup', (event) => {
	// Contains the element where the event happened
	console.log(event.target);
	// Gets the value of the input object
	console.log(event.target.value);
})*/

// Activity

txtFirstName.addEventListener('keyup',fullName);
txtLastName.addEventListener('keyup',fullName);
function fullName(){
	spanFullName.innerHTML =txtFirstName.value+" "+txtLastName.value
}
